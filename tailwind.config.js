const colors = require('./tailwind.colors.json')

module.exports = {
  mode: 'jit',
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        'homepage': { 'raw': '(min-width: 60rem)' },
        'small-homepage': { 'max': '60rem' }
      }
    },
    colors
  },
  variants: {
    extend: {}
  },
  plugins: [require('daisyui')],
  daisyui: {
    themes: [
      {'gaiaX': {
        "primary": colors['dark-blue'],
        "primary-focus": colors.blue,
        "primary-content": colors.white,
        "secondary": colors.violet,
        "secondary-focus": colors.purple,
        "secondary-content": colors.white,
        "accent": colors.cyan,
        "accent-focus": colors.cyan,
        "accent-content": colors.white,
        "base-100": colors.white,
        "base-200": "#f9fafb",
        "base-300": "#d1d5db",
        "base-content": "#1f2937",
        "info": colors.info,
        "success": colors.success,
        "error": colors.error
        }
      }
    ]
  },
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}'
  ]
}
