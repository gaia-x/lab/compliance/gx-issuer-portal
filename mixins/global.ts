import Vue from 'vue'

export default Vue.extend({
  methods: {
    navigateTo(path: string) {
      return this.$router.push({ path: this.localePath(path) })
    }
  }
})
